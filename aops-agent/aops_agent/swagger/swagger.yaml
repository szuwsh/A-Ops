swagger: "2.0"
info:
  description: "GroupDesc"
  version: "v1"
  title: "aops_agent"
basePath: "/v1"
tags:
- name: "aops_agent"
- name: "plugin"
schemes:
- "https"
paths:
  /agent/basic/info:
    get:
      tags:
      - "aops_agent"
      description: "获取主机基础信息"
      operationId: "get_host_info"
      parameters:
        - name: access_token
          in: header
          description: 授权码
          type: string
          required: true
      responses:
        "200":
          description: "主机的基本信息"
          schema:
            $ref: "#/definitions/get_host_info_rsp"
      x-swagger-router-controller: "aops_agent.controllers.agent_controller"
  /agent/plugin/start:
    post:
      tags:
        - plugin
      description: 启动插件。暂时不支持指定配置文件启动
      operationId: start_plugin
      parameters:
        - name: plugin_name
          in: query
          required: true
          description: 插件名
          type: string
        - name: access_token
          in: header
          description: 授权码
          type: string
          required: true
      responses:
        '200':
          description: ''
          schema:
            type: object
            properties:
              code:
                type: integer
              msg:
                type: string
      x-swagger-router-controller: "aops_agent.controllers.plugin_controller"
  /agent/plugin/stop:
    post:
      tags:
        - plugin
      description: 停止插件
      operationId: stop_plugin
      parameters:
        - name: plugin_name
          in: query
          required: true
          description: 插件名
          type: string
        - name: access_token
          in: header
          description: 授权码
          type: string
          required: true
      responses:
        '200':
          description: ''
          schema:
            type: object
            properties:
              code:
                type: integer
              msg:
                type: string
      x-swagger-router-controller: "aops_agent.controllers.plugin_controller"
  /agent/collect/items/change:
    post:
      tags:
        - plugin
      description: |-
        改变agent中各插件的采集项状态
      operationId: change_collect_items
      parameters:
        - name: collect_items_status
          in: body
          required: true
          description: 修改的采集项信息
          schema:
            $ref: '#/definitions/resp_collect_items_change'
        - name: access_token
          in: header
          description: 授权码
          type: string
          required: true
      responses:
        '200':
          description: '返回设置文件/配置失败的列表'
          schema:
            $ref: '#/definitions/resp_collect_items_change'
      x-swagger-router-controller: "aops_agent.controllers.plugin_controller"
  /agent/plugin/info:
    get:
      tags:
        - agent
      description: 获取agent插件运行信息
      operationId: agent_plugin_info
      parameters:
        - name: access_token
          in: header
          required: true
          description: 授权码
          type: string
      responses:
        '200':
          description: 获取插件运行信息响应
          schema:
            $ref: '#/definitions/resp_plugin_info'
        '400':
          description: 错误响应
          schema:
            $ref: '#/definitions/ErrorRsp'
      x-swagger-router-controller: "aops_agent.controllers.agent_controller"
  /agent/application/info:
    get:
      tags:
        - agent
      operationId: get_application_info
      parameters:
        - name: access_token
          in: header
          description: 授权码
          type: string
          required: true
      responses:
        '200':
          description: 获取agent当前运行的应用
          schema:
            $ref: '#/definitions/resp_application_info'
      x-swagger-router-controller: "aops_agent.controllers.agent_controller"
  /agent/file/collect:
    post:
      tags:
        - agent
      description: 获取系统特定配置文件信息。将文件压缩后传回
      operationId: collect_file
      parameters:
        - name: config_path_list
          in: body
          description: 需获取配置文件路径
          schema:
            type: array
            items:
              type: string
            example:
              - configpath1
              - configpath2
          required: true
        - name: access_token
          in: header
          description: 授权码
          type: string
          required: true
      responses:
        '200':
          description: 返回请求文件内容主体
          schema:
            $ref: '#/definitions/resp_collect_file'
      x-swagger-router-controller: "aops_agent.controllers.agent_controller"
definitions:
  ErrorRsp:
    required:
    - "error_code"
    - "error_msg"
    properties:
      error_code:
        type: "string"
        description: "错误码"
        minLength: 8
        maxLength: 36
      error_msg:
        type: "string"
        description: "错误描述"
        minLength: 2
        maxLength: 512
    description: "失败时返回的错误对象"
  get_host_info_rsp:
    description: "获取主机基本信息"
    properties:
      os:
        type: "object"
        description: "操作系统信息"
        properties:
          os_version:
            type: "string"
            description: "操作系统版本"
          kernel:
            type: "string"
            description: "kernel版本"
          bios_version:
            type: "string"
            description: "bios版本"
        required:
        - "kernel"
        - "os_version"
        - "bios_version"
  resp_collect_items_change:
    description: 采集状态修改结果
    properties:
      code:
        type: integer
      msg:
        type: string
      resp:
        additionalProperties:
          properties:
            success:
              type: array
              items:
                type: string
            failure:
              type: array
              items:
                type: string
  resp_plugin_info:
    description: 获取agent插件信息响应
    properties:
      code:
        type: integer
      msg:
        type: string
      resp:
        type: array
        items:
          type: object
          properties:
            plugin_name:
              description: 插件信息
              properties:
                is_installed:
                  type: boolean
                  description: 是否安装
                status:
                  type: string
                  description: 运行状态
                resource:
                  description: 资源占用值
                  type: array
                  items:
                    type: object
                    properties:
                      name:
                        type: string
                      limit_value:
                        type: string
                      current_value:
                        type: string
                collect_items:
                    type: array
                    items:
                      type: object
                      properties:
                        probe_name:
                          type: string
                        probe_status:
                          type: string
                        auto_support:
                          type: boolean
          required:
            - plugin_name
            - collect_items
            - resource
            - is_installed
            - status
  resp_application_info:
    description: 目标应用运行信息
    properties:
      code:
        type: integer
      msg:
        type: string
      resp:
        properties:
          running:
            type: array
            items:
              type: string
  resp_collect_file:
    properties:
      code:
        type: integer
      msg:
        type: string
      resp:
        type: array
        items:
          properties:
            host_id:
              type: string
              description: 主机ID
            success_files:
              type: array
              items:
                type: string
              description: 收集成功文件列表
            fail_files:
              type: array
              items:
                type: string
              description: 收集失败文件列表
            infos:
              description: 信息列表
              type: array
              items:
                type: object
                properties:
                  path:
                    type: string
                    description: 文件路径
                  file_attr:
                    description: 文件属性
                    properties:
                      mode:
                        type: string
                        description: 文件mode
                      owner:
                        type: string
                        description: 文件owner
                      group:
                        type: string
                        description: 文件group
                    required:
                      - mode
                      - owner
                      - group
                  content:
                    type: string
                    description: 文件内容
                required:
                  - path
                  - file_attr
                  - content
          required:
            - host_id
            - success_files
            - fail_files
            - infos
        description: 响应体